package ishell_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestIshell(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Ishell Suite")
}
