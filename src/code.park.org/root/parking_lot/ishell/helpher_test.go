package ishell

import (
	"fmt"

	"code.park.org/root/parking_lot/models"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("iShell Helpher functions", func() {
	Context("Test Process basic validations", func() {
		It("Invalid command to process", func() {
			AvailableCommandsHints()
			Greeting()
			inputCmd := "invalid_command \targs"
			cmd, err := Process(inputCmd)
			Expect(err).To(Equal(models.ErrInvalidTabSpace))
			Expect(cmd).To(BeNil())
		})
		It("Invalid command to process", func() {
			inputCmd := "invalid_command args"
			cmd, err := Process(inputCmd)
			Expect(err.Error()).To(Equal("Command 'invalid_command' is invalid!. Please type 'help' see available commands"))
			Expect(cmd).To(BeNil())
		})
	})
	Context("Test Process By valid command", func() {
		BeforeEach(func() {
			InitIshell()
		})
		AfterEach(func() {
			Store = nil
		})
		It("Invalid arguments for create_parking_lot ", func() {
			inputCmd := "create_parking_lot args1 args2"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "create_parking_lot", models.CMDArgumentLength["create_parking_lot"], 2)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create create_parking_lot cmd object", func() {
			inputCmd := "create_parking_lot args1"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("create_parking_lot"))
			Expect(len(cmd.Arguments)).To(Equal(1))
		})

		It("Invalid arguments for park", func() {
			inputCmd := "park args"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "park", models.CMDArgumentLength["park"], 1)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create park cmd object", func() {
			inputCmd := "park args1 args2"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("park"))
			Expect(len(cmd.Arguments)).To(Equal(2))
		})

		It("Invalid arguments for leave", func() {
			inputCmd := "leave"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "leave", models.CMDArgumentLength["leave"], 0)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create leave cmd object", func() {
			inputCmd := "leave args1"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("leave"))
			Expect(len(cmd.Arguments)).To(Equal(1))
		})

		It("Invalid arguments for status", func() {
			inputCmd := "status args"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "status", models.CMDArgumentLength["status"], 1)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create leave cmd object", func() {
			inputCmd := "status"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("status"))
			Expect(len(cmd.Arguments)).To(Equal(0))
		})

		It("Invalid arguments for registration_numbers_for_cars_with_colour", func() {
			inputCmd := "registration_numbers_for_cars_with_colour args1 arg2"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "registration_numbers_for_cars_with_colour", models.CMDArgumentLength["registration_numbers_for_cars_with_colour"], 2)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create registration_numbers_for_cars_with_colour cmd object", func() {
			inputCmd := "registration_numbers_for_cars_with_colour args1"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("registration_numbers_for_cars_with_colour"))
			Expect(len(cmd.Arguments)).To(Equal(1))
		})

		It("Invalid arguments for slot_numbers_for_cars_with_colour", func() {
			inputCmd := "slot_numbers_for_cars_with_colour args1 arg2"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "slot_numbers_for_cars_with_colour", models.CMDArgumentLength["slot_numbers_for_cars_with_colour"], 2)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create slot_numbers_for_cars_with_colour cmd object", func() {
			inputCmd := "slot_numbers_for_cars_with_colour args1"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("slot_numbers_for_cars_with_colour"))
			Expect(len(cmd.Arguments)).To(Equal(1))
		})

		It("Invalid arguments for slot_number_for_registration_number", func() {
			inputCmd := "slot_number_for_registration_number args arg2"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "slot_number_for_registration_number", models.CMDArgumentLength["slot_number_for_registration_number"], 2)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create slot_number_for_registration_number cmd object", func() {
			inputCmd := "slot_number_for_registration_number args1"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("slot_number_for_registration_number"))
			Expect(len(cmd.Arguments)).To(Equal(1))
		})

		It("Invalid arguments for help", func() {
			inputCmd := "help args"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "help", models.CMDArgumentLength["help"], 1)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create help cmd object", func() {
			inputCmd := "help"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("help"))
			Expect(len(cmd.Arguments)).To(Equal(0))
		})

		It("Invalid arguments for shell_history", func() {
			inputCmd := "shell_history args"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "shell_history", models.CMDArgumentLength["shell_history"], 1)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create shell_history cmd object", func() {
			inputCmd := "shell_history"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("shell_history"))
			Expect(len(cmd.Arguments)).To(Equal(0))
		})

		It("Invalid arguments for park_history", func() {
			inputCmd := "park_history args"
			cmd, err := Process(inputCmd)
			toEqual := fmt.Sprintf(models.ErrInsuffArguments, "park_history", models.CMDArgumentLength["park_history"], 1)
			Expect(err.Error()).To(Equal(toEqual))
			Expect(cmd).To(BeNil())
		})
		It("should create park_history cmd object", func() {
			inputCmd := "park_history"
			cmd, err := Process(inputCmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(cmd.GetName()).To(Equal("park_history"))
			Expect(len(cmd.Arguments)).To(Equal(0))
		})
	})
})
