package ishell

import (
	"bytes"

	. "github.com/onsi/ginkgo"
)

var _ = Describe("iShell Input Command File Process", func() {
	It("Should Fail - Invalid file", func() {
		bytes.NewBufferString("hello")
		Start()
	})
	Context("Test ProcessFile", func() {
		BeforeEach(func() {
			InitIshell()
		})
		AfterEach(func() {
			Store = nil
		})

		It("Should split command string", func() {
			bytes.NewBufferString("hello")
			Start()
		})
	})
})
