package ishell

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"code.park.org/root/parking_lot/models"
	"code.park.org/root/parking_lot/store"
	"code.park.org/root/parking_lot/utils"
)

var shell *models.IShell
var Store store.Store

func InitIshell() {
	Store = store.NewStore()
	shell = &models.IShell{
		Prompt: models.DefaultPrompt,
	}
}

// Start attempts to create new interactive session
func Start() {
	if shell == nil {
		fmt.Println(models.IShellWelcome)
		InitIshell()
	}
	shell.ShowPrompt()
	reader := bufio.NewReader(os.Stdin)
	for {
		cmdInputStr, err := reader.ReadString(utils.EndLineDelim)
		if err != nil {
			break
		}
		cmdInputStr = strings.TrimRight(cmdInputStr, utils.NewLineDelim)
		if strings.TrimSpace(cmdInputStr) != "" {
			shell.RecordHistory(cmdInputStr)
			// process the commands
			cmd, err := Process(cmdInputStr)
			if err != nil {
				fmt.Println(err.Error())
			}
			if cmd != nil {
				cmd.RecordShellHistory(shell.History)
				if cmd.IsExit() {
					break
				}
				// Execute the command
				response, err := cmd.Connection.Execute(cmd)
				if err != nil {
					fmt.Println(err.Error())
				}
				fmt.Println(response)
			}
		}
		shell.ShowPrompt()
	}
	Greeting()
}
