package ishell

import (
	"fmt"
	"strings"

	"code.park.org/root/parking_lot/models"
	"code.park.org/root/parking_lot/utils"
)

func AvailableCommandsHints() {
	fmt.Println(models.AllCommendHint)
}

func Greeting() {
	fmt.Println(models.GreetingText)
}

// Process attempts to validate the command input string.
// Checks the command and arguments list are valid or not.
func Process(cmdInputStr string) (*models.Command, error) {
	var cmd = new(models.Command)
	cmdInputList, err := utils.SplitCmdArguments(cmdInputStr)
	if err != nil {
		return nil, err
	}
	cmd.Command = cmdInputList[0]
	if len(cmdInputList) > 1 {
		cmd.Arguments = strings.Split(cmdInputList[1], utils.Space)
	}
	// validate the inputs
	if err := cmd.Ok(); err != nil {
		return nil, err
	}
	SetStoreConnection(cmd)
	return cmd, nil
}

func SetStoreConnection(command *models.Command) {
	switch command.Command {
	case string(models.CMDCreateParkingLot):
		command.Connection = Store.CreateParkingLot()
	case string(models.CMDPark):
		command.Connection = Store.Park()
	case string(models.CMDLeave):
		command.Connection = Store.Leave()
	case string(models.CMDStatus):
		command.Connection = Store.Status()
	case string(models.CMDRegNosForCarColour):
		command.Connection = Store.RegNosByColour()
	case string(models.CMDSlotNosForCarColour):
		command.Connection = Store.SlotNosByColor()
	case string(models.CMDSlotNoForRegNo):
		command.Connection = Store.SlotNoByRegNo()
	case string(models.CMDHelp):
		command.Connection = Store.Help()
	case string(models.CMDShellHistory):
		command.Connection = Store.ShellHistory()
	case string(models.CMDParkingHistory):
		command.Connection = Store.ParkHistory()
	}
}
