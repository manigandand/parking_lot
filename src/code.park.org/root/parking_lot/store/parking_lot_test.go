package store

import (
	"fmt"

	"code.park.org/root/parking_lot/models"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("parking lot store tests", func() {
	var (
		connection Store
	)
	connection = NewStore()
	It("Tear Down Store Data", func() {
		TearDown()
	})
	Context("parking_lot store excute", func() {
		TearDown()
		cmd := &models.Command{
			Command: "create_parking_lot",
		}
		It("create_parking_lot help", func() {
			cmd.Arguments = []string{"help"}
			res, err := connection.CreateParkingLot().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal(models.CMDCreateParkingLotHint))
		})

		It("invalid arguments string", func() {
			cmd.Arguments = []string{"assa"}
			res, err := connection.CreateParkingLot().Execute(cmd)
			Expect(err).To(Equal(models.ErrInvalidInputSlot))
			Expect(res).To(Equal(""))
		})

		It("Invalid total slot count", func() {
			cmd.Arguments = []string{"0"}
			res, err := connection.CreateParkingLot().Execute(cmd)
			Expect(err).To(Equal(models.ErrInvalidSlotCount(0)))
			Expect(res).To(Equal(""))
		})

		It("Create a parking lot with 5 slots", func() {
			cmd.Arguments = []string{"5"}
			res, err := connection.CreateParkingLot().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal(fmt.Sprintf(ParkinglotCreatedInfo, 5)))
		})

		It("parking lot already created", func() {
			cmd.Arguments = []string{"5"}
			res, err := connection.CreateParkingLot().Execute(cmd)
			Expect(err).To(Equal(models.ErrParkingLotAlreadyCreated))
			Expect(res).To(Equal(""))
		})
	})
})
