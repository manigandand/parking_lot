package store

import (
	"fmt"

	"code.park.org/root/parking_lot/models"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("parking lot store tests", func() {
	var (
		connection Store
	)
	connection = NewStore()
	It("Tear Down Store Data", func() {
		TearDown()
	})

	Context("parking_lot store execute", func() {
		TearDown()

		It("park help", func() {
			cmd := &models.Command{
				Command:   "slot_number_for_registration_number",
				Arguments: []string{"help"},
			}
			res, err := connection.SlotNoByRegNo().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal(models.CMDSlotNoForRegNoHint))
		})
		It("No parking lot available", func() {
			cmd := &models.Command{
				Command:   "slot_number_for_registration_number",
				Arguments: []string{"123"},
			}
			res, err := connection.SlotNoByRegNo().Execute(cmd)
			Expect(err).To(Equal(models.ErrNoParkingLot))
			Expect(res).To(Equal(""))
		})

		It("Create a parking lot with 1 slots", func() {
			cmd := &models.Command{
				Command:   "create_parking_lot",
				Arguments: []string{"2"},
			}
			res, err := connection.CreateParkingLot().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal(fmt.Sprintf(ParkinglotCreatedInfo, 2)))
		})

		It("invalid arguments registration number", func() {
			cmd := &models.Command{
				Command:   "slot_number_for_registration_number",
				Arguments: []string{"k2a-123-21-s123%"},
			}
			res, err := connection.SlotNoByRegNo().Execute(cmd)
			Expect(err).To(Equal(models.ErrInvalidRegNo))
			Expect(res).To(Equal(""))
		})

		It("park a vehicle", func() {
			cmd := &models.Command{
				Command:   "park",
				Arguments: []string{"TN-24-AJ-8462", "Red"},
			}
			res, err := connection.Park().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal("Allocated slot number: 1"))
		})

		It("Get slot number by registration number - No cars", func() {
			cmd := &models.Command{
				Command:   "slot_number_for_registration_number",
				Arguments: []string{"TN-24-AJ-8467"},
			}
			res, err := connection.SlotNoByRegNo().Execute(cmd)
			Expect(err).To(Equal(models.ErrCarNotFound))
			Expect(res).To(Equal(""))
		})

		It("Get slot number by registration number", func() {
			cmd := &models.Command{
				Command:   "slot_number_for_registration_number",
				Arguments: []string{"TN-24-AJ-8462"},
			}
			res, err := connection.SlotNoByRegNo().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal("1"))
		})
	})
})
