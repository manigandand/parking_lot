package store

import (
	"fmt"
	"strconv"

	"code.park.org/root/parking_lot/models"
)

const parkingLotName = "go-jek-lot1"

type createParkingLotStore struct {
	*store
}

func NewCreateParkingLotStore(st *store) *createParkingLotStore {
	pl := &createParkingLotStore{st}
	return pl
}

func (pl *createParkingLotStore) IsHelp(arg string) (string, bool) {
	if arg == string(models.CMDHelp) {
		return models.CMDCreateParkingLotHint, true
	}
	return "", false
}

// Execute - this will create_parking_lot with given slots.
// The system will check if no parking_lot availabe then it create a parking_lot
// with N slots.
// All the slots will initialized with sequence slot numbers by start 1 to N
func (pl *createParkingLotStore) Execute(cmd *models.Command) (string, error) {
	if res, isHelp := pl.IsHelp(cmd.Arguments[0]); isHelp {
		return res, nil
	}
	totalSlots, err := strconv.Atoi(cmd.Arguments[0])
	if err != nil {
		return "", models.ErrInvalidInputSlot
	}
	if totalSlots <= 0 {
		return "", models.ErrInvalidSlotCount(totalSlots)
	}
	if ParkingLot == nil {
		pl := new(models.ParkingLot)
		pl.Name = parkingLotName
		pl.TotalSlots = totalSlots
		pl.Slots = make([]*models.Slot, totalSlots)
		// initiate nil slot properties
		for i := range pl.Slots {
			pl.Slots[i] = new(models.Slot)
			pl.Slots[i].SetID(i + 1)
			pl.Slots[i].SetName(i + 1)
			pl.Slots[i].MakeSlotFree()
		}
		// set parking lot info global
		ParkingLot = pl
		return fmt.Sprintf(ParkinglotCreatedInfo, totalSlots), nil
	}

	return "", models.ErrParkingLotAlreadyCreated
}
