package store

import (
	"fmt"
	"strings"

	"code.park.org/root/parking_lot/models"
	"code.park.org/root/parking_lot/utils"
)

type slotNosByColorStore struct {
	*store
}

func NewSlotNosByColorStore(st *store) *slotNosByColorStore {
	pl := &slotNosByColorStore{st}
	return pl
}

func (pl *slotNosByColorStore) IsHelp(arg string) (string, bool) {
	if arg == string(models.CMDHelp) {
		return models.CMDSlotNosForCarColourHint, true
	}
	return "", false
}

// Execute returns all the slot numbers of the vehicle parked by the colour.
func (pl *slotNosByColorStore) Execute(cmd *models.Command) (string, error) {
	var slotNos []string
	if res, isHelp := pl.IsHelp(cmd.Arguments[0]); isHelp {
		return res, nil
	}
	if ParkingLot == nil {
		return "", models.ErrNoParkingLot
	}
	// validate the request
	if !utils.IsValidString(cmd.Arguments[0]) {
		return "", models.ErrInvalidColour
	}
	for _, slot := range ParkingLot.Slots {
		if slot.IsSlotOccupied() {
			if slot.Vehicle.IsVehicleColurMatched(cmd.Arguments[0]) {
				slotNos = append(slotNos, fmt.Sprintf("%v", slot.GetID()))
			}
		}
	}

	if len(slotNos) == 0 {
		return "", models.ErrNoCarFoundByColour(cmd.Arguments[0])
	}

	return strings.Join(slotNos, ","), nil
}
