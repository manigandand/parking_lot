package store

import (
	"fmt"

	"code.park.org/root/parking_lot/models"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("parking lot store tests", func() {
	var (
		connection Store
	)
	connection = NewStore()
	It("Tear Down Store Data", func() {
		TearDown()
	})

	Context("parking_lot store execute", func() {
		TearDown()

		It("park help", func() {
			cmd := &models.Command{
				Command:   "registration_numbers_for_cars_with_colour",
				Arguments: []string{"help"},
			}
			res, err := connection.RegNosByColour().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal(models.CMDRegNosForCarColourHint))
		})
		It("No parking lot available", func() {
			cmd := &models.Command{
				Command:   "registration_numbers_for_cars_with_colour",
				Arguments: []string{"123"},
			}
			res, err := connection.RegNosByColour().Execute(cmd)
			Expect(err).To(Equal(models.ErrNoParkingLot))
			Expect(res).To(Equal(""))
		})

		It("Create a parking lot with 1 slots", func() {
			cmd := &models.Command{
				Command:   "create_parking_lot",
				Arguments: []string{"2"},
			}
			res, err := connection.CreateParkingLot().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal(fmt.Sprintf(ParkinglotCreatedInfo, 2)))
		})

		It("invalid arguments colour", func() {
			cmd := &models.Command{
				Command:   "registration_numbers_for_cars_with_colour",
				Arguments: []string{"k2a-123-21-s123%"},
			}
			res, err := connection.RegNosByColour().Execute(cmd)
			Expect(err).To(Equal(models.ErrInvalidColour))
			Expect(res).To(Equal(""))
		})

		It("park a vehicle", func() {
			cmd := &models.Command{
				Command:   "park",
				Arguments: []string{"TN-24-AJ-8462", "Red"},
			}
			res, err := connection.Park().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal("Allocated slot number: 1"))

			cmd2 := &models.Command{
				Command:   "park",
				Arguments: []string{"TN-24-AJ-8465", "Red"},
			}
			res, err = connection.Park().Execute(cmd2)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal("Allocated slot number: 2"))
		})
		It("Get Registration numbers by colour - No cars", func() {
			cmd := &models.Command{
				Command:   "registration_numbers_for_cars_with_colour",
				Arguments: []string{"Black"},
			}
			res, err := connection.RegNosByColour().Execute(cmd)
			Expect(err).To(Equal(models.ErrNoCarFoundByColour(cmd.Arguments[0])))
			Expect(res).To(Equal(""))
		})

		It("Get Registration numbers by colour", func() {
			cmd := &models.Command{
				Command:   "registration_numbers_for_cars_with_colour",
				Arguments: []string{"Red"},
			}
			res, err := connection.RegNosByColour().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal("TN-24-AJ-8462,TN-24-AJ-8465"))
		})
	})
})
