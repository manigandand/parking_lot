package store

import (
	"strings"

	"code.park.org/root/parking_lot/models"
	"code.park.org/root/parking_lot/utils"
)

type regNosByColourStore struct {
	*store
}

func NewRegNosByColourStore(st *store) *regNosByColourStore {
	pl := &regNosByColourStore{st}
	return pl
}

func (pl *regNosByColourStore) IsHelp(arg string) (string, bool) {
	if arg == string(models.CMDHelp) {
		return models.CMDRegNosForCarColourHint, true
	}
	return "", false
}

// Execute returns all the registration numbers of the vehicles which parked by colour.
func (pl *regNosByColourStore) Execute(cmd *models.Command) (string, error) {
	var regNos []string
	if res, isHelp := pl.IsHelp(cmd.Arguments[0]); isHelp {
		return res, nil
	}
	if ParkingLot == nil {
		return "", models.ErrNoParkingLot
	}
	// validate the request
	if !utils.IsValidString(cmd.Arguments[0]) {
		return "", models.ErrInvalidColour
	}

	for _, slot := range ParkingLot.Slots {
		if slot.IsSlotOccupied() {
			if slot.Vehicle.IsVehicleColurMatched(cmd.Arguments[0]) {
				regNos = append(regNos, slot.Vehicle.GetRegNumber())
			}
		}
	}
	if len(regNos) == 0 {
		return "", models.ErrNoCarFoundByColour(cmd.Arguments[0])
	}

	return strings.Join(regNos, ","), nil
}
