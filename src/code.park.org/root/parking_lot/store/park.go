package store

import (
	"fmt"
	"strings"
	"time"

	"code.park.org/root/parking_lot/models"
	"code.park.org/root/parking_lot/utils"
)

type parkStore struct {
	*store
}

func NewParkStore(st *store) *parkStore {
	pl := &parkStore{st}
	return pl
}

func (pl *parkStore) IsHelp(arg string) (string, bool) {
	if arg == string(models.CMDHelp) {
		return models.CMDParkHint, true
	}
	return "", false
}

// Execute - `park` Command will takes registration number and colour as Arguments
// the system checks for a first availabe slot to park, if slot available
// slot will allocated to the vehicle.
// This will checks if the vehicle registration number is duplicate or not.
func (pl *parkStore) Execute(cmd *models.Command) (string, error) {
	if res, isHelp := pl.IsHelp(cmd.Arguments[0]); isHelp {
		return res, nil
	}
	if ParkingLot == nil {
		return "", models.ErrNoParkingLot
	}
	if err := validateParkReq(cmd.Arguments); err != nil {
		return "", err
	}
	// check for registration number deplication
	if isCarAlreadyParked(cmd.Arguments[0]) {
		return "", models.ErrDuplicateVehicle(cmd.Arguments[0])
	}
	// Checks for first available slot
	car := &models.Vehicle{
		RegistrationNumber: cmd.Arguments[0],
		Colour:             strings.ToLower(cmd.Arguments[1]),
	}
	availSlot, err := ParkingLot.FirstAvailableSlot()
	if err != nil {
		return "", err
	}
	// park vehicle in the slot
	if err := availSlot.ParkVehicle(car); err != nil {
		return "", err
	}
	parkHistory := &models.ParkHistory{
		SlotID:             availSlot.GetID(),
		RegistrationNumber: cmd.Arguments[0],
		Colour:             strings.ToLower(cmd.Arguments[1]),
		CreatedAt:          time.Now(),
	}
	// save parking history
	ParkingLot.ParkHistory = append(ParkingLot.ParkHistory, parkHistory)

	return fmt.Sprintf(SlotAllocatedInfo, availSlot.GetID()), nil
}

func validateParkReq(args []string) error {
	if !utils.IsRegNoValid(args[0]) {
		return models.ErrInvalidRegNo
	}
	if !utils.IsValidString(args[1]) {
		return models.ErrInvalidColour
	}
	return nil
}

func isCarAlreadyParked(regNo string) bool {
	for _, slot := range ParkingLot.Slots {
		if slot.IsSlotOccupied() {
			if slot.Vehicle.IsVehicleRegNoMatched(regNo) {
				return true
			}
		}
	}

	return false
}
