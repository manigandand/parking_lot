package store

import (
	"fmt"
	"strconv"

	"code.park.org/root/parking_lot/models"
)

type leaveStore struct {
	*store
}

func NewLeaveStore(st *store) *leaveStore {
	pl := &leaveStore{st}
	return pl
}

func (pl *leaveStore) IsHelp(arg string) (string, bool) {
	if arg == string(models.CMDHelp) {
		return models.CMDLeaveHint, true
	}
	return "", false
}

// Execute - `leave` this takes slotID as Argument and checks if the slotID is
// valid or not. If valid, it will make the slot as free.
func (pl *leaveStore) Execute(cmd *models.Command) (string, error) {
	if res, isHelp := pl.IsHelp(cmd.Arguments[0]); isHelp {
		return res, nil
	}
	if ParkingLot == nil {
		return "", models.ErrNoParkingLot
	}
	slotID, err := strconv.Atoi(cmd.Arguments[0])
	if err != nil {
		return "", models.ErrInvalidInputSlot
	}
	if slotID == 0 || slotID > ParkingLot.TotalSlots {
		return "", models.ErrInvalidSlotID
	}
	// make slot free
	slot := ParkingLot.Slots[slotID-1]
	if err := slot.ExitPark(); err != nil {
		return "", err
	}

	return fmt.Sprintf(SlotIsFreeInfo, slotID), nil
}
