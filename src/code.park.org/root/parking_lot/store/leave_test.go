package store

import (
	"fmt"

	"code.park.org/root/parking_lot/models"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("parking lot store tests", func() {
	var (
		connection Store
	)
	connection = NewStore()
	It("Tear Down Store Data", func() {
		TearDown()
	})

	Context("parking_lot store execute", func() {
		TearDown()

		It("park help", func() {
			cmd := &models.Command{
				Command:   "leave",
				Arguments: []string{"help"},
			}
			res, err := connection.Leave().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal(models.CMDLeaveHint))
		})
		It("No parking lot available", func() {
			cmd := &models.Command{
				Command:   "leave",
				Arguments: []string{"123"},
			}
			res, err := connection.Leave().Execute(cmd)
			Expect(err).To(Equal(models.ErrNoParkingLot))
			Expect(res).To(Equal(""))
		})

		It("Create a parking lot with 5 slots", func() {
			cmd := &models.Command{
				Command:   "create_parking_lot",
				Arguments: []string{"1"},
			}
			res, err := connection.CreateParkingLot().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal(fmt.Sprintf(ParkinglotCreatedInfo, 1)))
		})

		It("invalid arguments slot id", func() {
			cmd := &models.Command{
				Command:   "leave",
				Arguments: []string{"k2a-123-21-s123%"},
			}
			res, err := connection.Leave().Execute(cmd)
			Expect(err).To(Equal(models.ErrInvalidInputSlot))
			Expect(res).To(Equal(""))
		})
		It("invalid arguments slot number", func() {
			cmd := &models.Command{
				Command:   "leave",
				Arguments: []string{"0"},
			}
			res, err := connection.Leave().Execute(cmd)
			Expect(err).To(Equal(models.ErrInvalidSlotID))
			Expect(res).To(Equal(""))
		})

		It("park a vehicle", func() {
			cmd := &models.Command{
				Command:   "park",
				Arguments: []string{"TN-24-AJ-8462", "Red"},
			}
			res, err := connection.Park().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal("Allocated slot number: 1"))
		})

		It("Leave from the parking slot", func() {
			cmd := &models.Command{
				Command:   "leave",
				Arguments: []string{"1"},
			}
			res, err := connection.Leave().Execute(cmd)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(res).To(Equal(fmt.Sprintf(SlotIsFreeInfo, 1)))
		})
	})
})
