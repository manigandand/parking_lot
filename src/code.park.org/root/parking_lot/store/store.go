package store

import "code.park.org/root/parking_lot/models"

var (
	ParkinglotCreatedInfo = "Created​ a parking​ lot with %d slots"
	SlotAllocatedInfo     = "Allocated slot number: %v"
	SlotIsFreeInfo        = "Slot number %v is free"
)

// ParkingLot holds the all parking data
var ParkingLot *models.ParkingLot

type store struct {
	// db               *gorm.DB
	createParkingLot models.CMDStore
	park             models.CMDStore
	leave            models.CMDStore
	status           models.CMDStore
	regNosByColour   models.CMDStore
	slotNosByColor   models.CMDStore
	slotNoByRegNo    models.CMDStore
	help             models.CMDStore
	shellHistory     models.CMDStore
	parkHistory      models.CMDStore
}

func (s store) CreateParkingLot() models.CMDStore {
	return s.createParkingLot
}
func (s store) Park() models.CMDStore {
	return s.park
}
func (s store) Leave() models.CMDStore {
	return s.leave
}
func (s store) Status() models.CMDStore {
	return s.status
}
func (s store) RegNosByColour() models.CMDStore {
	return s.regNosByColour
}
func (s store) SlotNosByColor() models.CMDStore {
	return s.slotNosByColor
}
func (s store) SlotNoByRegNo() models.CMDStore {
	return s.slotNoByRegNo
}
func (s store) Help() models.CMDStore {
	return s.help
}
func (s store) ShellHistory() models.CMDStore {
	return s.shellHistory
}

func (s store) ParkHistory() models.CMDStore {
	return s.parkHistory
}

func NewStore() *store {
	st := InitStore()
	st.createParkingLot = NewCreateParkingLotStore(st)
	st.park = NewParkStore(st)
	st.leave = NewLeaveStore(st)
	st.status = NewStatusStore(st)
	st.regNosByColour = NewRegNosByColourStore(st)
	st.slotNosByColor = NewSlotNosByColorStore(st)
	st.slotNoByRegNo = NewSlotNoByRegNoStore(st)
	st.help = NewHelpStore(st)
	st.shellHistory = NewShellHistoryStore(st)
	st.parkHistory = NewParkHistoryStore(st)

	return st
}

func InitStore() *store {
	return new(store)
}
