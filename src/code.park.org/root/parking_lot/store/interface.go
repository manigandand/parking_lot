package store

import (
	"fmt"
	"strings"

	"code.park.org/root/parking_lot/models"
	"code.park.org/root/parking_lot/utils"
)

type Store interface {
	// db *gorm.DB
	CreateParkingLot() models.CMDStore
	Park() models.CMDStore
	Leave() models.CMDStore
	Status() models.CMDStore
	RegNosByColour() models.CMDStore
	SlotNosByColor() models.CMDStore
	SlotNoByRegNo() models.CMDStore
	Help() models.CMDStore
	ShellHistory() models.CMDStore
	ParkHistory() models.CMDStore
}

// help interface
type helpStore struct {
	*store
}

func NewHelpStore(st *store) *helpStore {
	h := &helpStore{st}
	return h
}

func (h *helpStore) Execute(cmd *models.Command) (string, error) {
	return models.AllCommendHint, nil
}

// Park History
type parkHistoryStore struct {
	*store
}

func NewParkHistoryStore(st *store) *parkHistoryStore {
	pl := &parkHistoryStore{st}
	return pl
}

func (pl *parkHistoryStore) Execute(cmd *models.Command) (string, error) {
	if ParkingLot == nil {
		return "", models.ErrNoParkingLot
	}
	if len(ParkingLot.ParkHistory) == 0 {
		return "", models.ErrNoHistoyFound("parking")
	}
	var parkHistory = []string{fmt.Sprintf("%-5s%-10s%-25s%-10s%-10s", "No.", "Slot No", "Registration Number", "Colour", "CreatedAt")}
	for i, history := range ParkingLot.ParkHistory {
		parkHistory = append(parkHistory, fmt.Sprintf("%-5d%-10d%-25s%-10s%-10s",
			i+1, history.SlotID, history.RegistrationNumber, strings.Title(history.Colour), utils.FormatDateTime(history.CreatedAt)))
	}
	return strings.Join(parkHistory, utils.NewLineDelim), nil
}

// shell History interface
type shellHistoryStore struct {
	*store
}

func NewShellHistoryStore(st *store) *shellHistoryStore {
	pl := &shellHistoryStore{st}
	return pl
}

func (pl *shellHistoryStore) Execute(cmd *models.Command) (string, error) {
	if len(cmd.ShellHistory) == 0 {
		return "", models.ErrNoHistoyFound("shell")
	}
	var slotHistory = []string{fmt.Sprintf("%-5s%-50s\t%-10s", "No.", "Command", "CreatedAt")}
	for i, history := range cmd.ShellHistory {
		slotHistory = append(slotHistory, fmt.Sprintf("%-5d%-50s\t%-10s", i+1, history.Command, utils.FormatDateTime(history.CreatedAt)))
	}
	return strings.Join(slotHistory, utils.NewLineDelim), nil
}
