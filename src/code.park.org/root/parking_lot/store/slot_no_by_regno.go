package store

import (
	"fmt"

	"code.park.org/root/parking_lot/models"
	"code.park.org/root/parking_lot/utils"
)

type slotNoByRegNoStore struct {
	*store
}

func NewSlotNoByRegNoStore(st *store) *slotNoByRegNoStore {
	pl := &slotNoByRegNoStore{st}
	return pl
}

func (pl *slotNoByRegNoStore) IsHelp(arg string) (string, bool) {
	if arg == string(models.CMDHelp) {
		return models.CMDSlotNoForRegNoHint, true
	}
	return "", false
}

// Execute returns slot number of the vehicle parked by registration number.
// If no vehicle found returns error.
func (pl *slotNoByRegNoStore) Execute(cmd *models.Command) (string, error) {
	var slotNo string
	var isMatched bool
	if res, isHelp := pl.IsHelp(cmd.Arguments[0]); isHelp {
		return res, nil
	}
	if ParkingLot == nil {
		return "", models.ErrNoParkingLot
	}
	// validate the request
	if !utils.IsRegNoValid(cmd.Arguments[0]) {
		return "", models.ErrInvalidRegNo
	}
	for _, slot := range ParkingLot.Slots {
		if slot.IsSlotOccupied() {
			if slot.Vehicle.IsVehicleRegNoMatched(cmd.Arguments[0]) {
				isMatched = true
				slotNo = fmt.Sprintf("%v", slot.GetID())
			}
		}
	}
	if !isMatched {
		return "", models.ErrCarNotFound
	}

	return slotNo, nil
}
