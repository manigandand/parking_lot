package models

import (
	"fmt"
	"time"
)

const (
	// DefaultPrompt is string prefix.
	DefaultPrompt = ">>>"
)

type IShellHistory struct {
	Command   string
	CreatedAt time.Time
}

// IShell is an interactive cli shell.
type IShell struct {
	Prompt  string
	History []*IShellHistory
}

// ShowPrompt displays the DefaultPrompt, and wait for the next commands
func (s *IShell) ShowPrompt() {
	fmt.Print(s.Prompt)
}

// RecordHistory holds the History for shell session
func (shell *IShell) RecordHistory(cmd string) {
	record := &IShellHistory{
		Command:   cmd,
		CreatedAt: time.Now(),
	}
	shell.History = append(shell.History, record)
}

// RecordHistory holds the History for shell session
func (c *Command) RecordShellHistory(history []*IShellHistory) {
	c.ShellHistory = history
}
