package models

type (
	//CMDType holds the type of commands
	CMDType = string
)

const (
	// CMDCreateParkingLot command input for create parking lot
	CMDCreateParkingLot CMDType = "create_parking_lot"
	// CMDPark command input for park a car
	CMDPark CMDType = "park"
	// CMDLeave command input for leave from parked slot
	CMDLeave CMDType = "leave"
	// CMDstatus command input for get current status of all parking lots
	CMDStatus CMDType = "status"
	// CMDRegNosForCarColour command input to get all the car registration no's by color
	CMDRegNosForCarColour CMDType = "registration_numbers_for_cars_with_colour"
	// CMDSlotNosForCarColour command input to get all the slot no's by car color
	CMDSlotNosForCarColour CMDType = "slot_numbers_for_cars_with_colour"
	// CMDSlotNoForRegNo command input to get the slot no by car registration no
	CMDSlotNoForRegNo CMDType = "slot_number_for_registration_number"
	// CMDHelp command input for get help hint for all the commands
	CMDHelp CMDType = "help"
	// CMDExit command input to exit from the interactive shell
	CMDExit CMDType = "exit"
	// CMDShellHistory command input to get all the interactive shell histories
	CMDShellHistory CMDType = "shell_history"
	// CMDShellHistory command input to get all the interactive shell histories
	CMDParkingHistory CMDType = "park_history"
)

// ValidCommandsByName holds the valid commands map
var ValidCommandsByName = map[string]bool{
	string(CMDCreateParkingLot):    true,
	string(CMDPark):                true,
	string(CMDLeave):               true,
	string(CMDStatus):              true,
	string(CMDRegNosForCarColour):  true,
	string(CMDSlotNosForCarColour): true,
	string(CMDSlotNoForRegNo):      true,
	string(CMDHelp):                true,
	string(CMDExit):                true,
	string(CMDShellHistory):        true,
	string(CMDParkingHistory):      true,
}

// CMDArgumentLength holds the exact arguments length to read for commands
var CMDArgumentLength = map[string]int{
	string(CMDCreateParkingLot):    1,
	string(CMDPark):                2,
	string(CMDLeave):               1,
	string(CMDStatus):              0,
	string(CMDRegNosForCarColour):  1,
	string(CMDSlotNosForCarColour): 1,
	string(CMDSlotNoForRegNo):      1,
	string(CMDHelp):                0,
	string(CMDExit):                0,
	string(CMDShellHistory):        0,
	string(CMDParkingHistory):      0,
}

type CMDStore interface {
	Execute(cmd *Command) (string, error)
}

// Command holds the command properties
type Command struct {
	Command      string
	Arguments    []string
	Connection   CMDStore
	ShellHistory []*IShellHistory
}

func (cmd *Command) GetName() string {
	return cmd.Command
}

func (cmd *Command) GetArguments() []string {
	return cmd.Arguments
}

func (cmd *Command) IsExit() bool {
	return cmd.GetName() == string(CMDExit)
}

// Ok validate the command object by name and arguments list
func (cmd *Command) Ok() error {
	if _, ok := ValidCommandsByName[cmd.Command]; !ok {
		return ErrInvalidCommand(cmd.Command)
	}
	if len(cmd.Arguments) != CMDArgumentLength[cmd.Command] {
		return ErrInvalidArguments(cmd.Command, CMDArgumentLength[cmd.Command], len(cmd.Arguments))
	}

	return nil
}
