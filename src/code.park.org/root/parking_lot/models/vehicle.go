package models

import "strings"

// Vehicle holds all the Vehicle properties
type Vehicle struct {
	RegistrationNumber string `json:"registration_type"`
	Colour             string `json:"colour"`
}

func (v *Vehicle) GetRegNumber() string {
	return v.RegistrationNumber
}

func (v *Vehicle) GetColour() string {
	return v.Colour
}

func (v *Vehicle) SetRegNumber(regNo string) {
	v.RegistrationNumber = regNo
	return
}

func (v *Vehicle) SetColour(colour string) {
	v.Colour = colour
	return
}

func (v *Vehicle) IsVehicleColurMatched(colour string) bool {
	return (v.Colour == strings.ToLower(colour))
}

func (v *Vehicle) IsVehicleRegNoMatched(regNO string) bool {
	return (v.RegistrationNumber == regNO)
}
