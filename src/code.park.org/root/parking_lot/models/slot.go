package models

import "fmt"

// Slot holds all the slot properties
type Slot struct {
	ID      uint     `json:"id"`
	Name    string   `json:"name"`
	IsFree  bool     `json:"is_free"`
	Vehicle *Vehicle `json:"vehicle"`
}

func (s *Slot) GetID() uint {
	return s.ID
}

func (s *Slot) GetName() string {
	return s.Name
}

func (s *Slot) SetID(ID int) {
	s.ID = uint(ID)
	return
}

func (s *Slot) MakeSlotFree() {
	s.IsFree = true
	return
}

func (s *Slot) SetSlotOccupied() {
	s.IsFree = false
	return
}

func (s *Slot) SetName(ID int) {
	s.Name = fmt.Sprintf("slot_%d", ID)
	return
}

func (s *Slot) IsSlotAvailable() bool {
	return (s.IsFree && s.Vehicle == nil)
}

func (s *Slot) IsSlotOccupied() bool {
	return (s.Vehicle != nil)
}

func (s *Slot) ParkVehicle(car *Vehicle) error {
	if s.Vehicle != nil {
		return ErrSlotAlreadyOccupied
	}
	// park vehicle here, make slot occupied
	s.SetSlotOccupied()
	s.Vehicle = car
	return nil
}

func (s *Slot) ExitPark() error {
	if s.Vehicle == nil {
		return ErrSlotAlreadyAvailable
	}
	s.MakeSlotFree()
	s.Vehicle = nil
	return nil
}
