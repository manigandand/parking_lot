package models

import (
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("models commands", func() {
	Context("Test ProcessFile", func() {
		It("ErrInvalidSlotCount", func() {
			res := ErrInvalidSlotCount(1)
			Expect(res.Error()).To(Equal(fmt.Sprintf(ErrInsuffSlot, 1)))
		})
		It("ErrNoCarFoundByColour", func() {
			res := ErrNoCarFoundByColour("red")
			Expect(res.Error()).To(Equal(fmt.Sprintf(NoCarFoundByColour, "red")))
		})
		It("ErrNoHistoyFound", func() {
			res := ErrNoHistoyFound("shell")
			Expect(res.Error()).To(Equal(fmt.Sprintf(NoHistoryFound, "shell")))
		})
		It("ErrDuplicateVehicle", func() {
			res := ErrDuplicateVehicle("ka-01-aj-1234")
			Expect(res.Error()).To(Equal(fmt.Sprintf(DuplicateVehicle, "ka-01-aj-1234")))
		})
	})
})
