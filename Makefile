# Makefile

GO_CMD=go
GO_BUILD=$(GO_CMD) build
GO_BUILD_RACE=$(GO_CMD) build -race
GO_TEST=$(GO_CMD) test
GO_TEST_VERBOSE=$(GO_CMD) test -v
GO_TEST_COVER=$(GO_CMD) test -cover
GO_INSTALL=$(GO_CMD) install -v

SERVER_BIN=parking_lot_server
SERVER_DIR=./src/code.park.org/root/parking_lot
SERVER_MAIN=main.go

SOURCE_PKG_DIR= code.park.org/root/parking_lot
SOURCEDIR=.
SOURCES := $(shell find $(SOURCEDIR) -name '*.go')

all: test build-server run

build-server:
	@echo "==> Building server ...";
	@$(GO_BUILD) -o $(SERVER_BIN) $(SERVER_DIR)/$(SERVER_MAIN) || exit 1;
	@chmod 755 $(SERVER_BIN)

test:
	@echo "==> Running tests ...";
	@$(GO_TEST_COVER) $(SOURCE_PKG_DIR)/utils
	@$(GO_TEST_COVER) $(SOURCE_PKG_DIR)/ishell
	@$(GO_TEST_COVER) $(SOURCE_PKG_DIR)/models
	@$(GO_TEST_COVER) $(SOURCE_PKG_DIR)/store

run:
	./$(SERVER_BIN) ${FILE}
