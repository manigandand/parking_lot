### Parking lot

Parking-Lot Solution in GO.

Usage:
        You can create a parking lot with N slots, and you can park the vehicle
in the alloted slot. Tou can even interact with the shell by using the following
commands to get status of slots, parked vehicle info, shell command histories, parking histories, etc..

Available commands:
```
    ●   create_parking_lot
            To create a parking lot with N slots.
            'create_parking_lot {no.of slots to create}'
            Eg: 'create_parking_lot 6'
            Eg: 'create_parking_lot help' to get help
    ●   park
            To park a vehicle, the system will allocate parking slot to park.
            'park {registration number} { vehicle colur}'
            Eg: 'park​ KA-01-HH-1234​ ​White'
            Eg: 'park help' to get help
    ●   leave
            To leave from the parking lot. After exit the slot will make as free.
            'leave {slot ID}'
            Eg: 'leave 1'
            Eg: 'leave help' to get help
    ●   status
            To get the current status of the all parking slots.
            Eg: 'status'
    ●   registration_numbers_for_cars_with_colour
            To get all the parked vehicle registration numbers by vehicle colour.
            'registration_numbers_for_cars_with_colour {vehicle colour}'
            Eg: 'registration_numbers_for_cars_with_colour Red'
            Eg: 'registration_numbers_for_cars_with_colour help' to get help
    ●   slot_numbers_for_cars_with_colour
            To get the slot numbers of all parked vehicles by vehicle colour.
            'slot_numbers_for_cars_with_colour {vehicle colour}'
            Eg: 'slot_numbers_for_cars_with_colour White'
            Eg: 'slot_numbers_for_cars_with_colour help' to get help
    ●   slot_number_for_registration_number
            To get the parked slot number by vehicle registration number.
            'slot_number_for_registration_number {registration number}'
            Eg: 'slot_number_for_registration_number KA-02-AJ-9999'
            Eg: 'slot_number_for_registration_number help' to get help
    ●   help
            To get all the availabe commands to use.
            Eg: 'help'
    ●   shell_history
            To get all the list of commands get used with in shell.
            Eg: 'shell_history'
    ●   park_history
            To get all the list of parking happend.
            Eg: 'park_history'
    ●   exit
            To exit from the current iShell.
            Eg: 'exit'
```

### How to run

By running `./parking_lot <input_file_path optional!>` file you can use get access
with this interactive shell to play.
This will run all the test files, build binaries and run the binaries.

Or you can also run `./parking_lot_server <input_file_path optional!>` to get access.
